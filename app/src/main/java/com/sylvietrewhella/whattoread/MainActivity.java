package com.sylvietrewhella.whattoread;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sylvietrewhella.whattoread.AsyncTaskProcess.DownloadHorizontalView;


public class MainActivity extends AppCompatActivity {

//    private Toolbar mTopToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialiseUI();

//        mTopToolbar = (Toolbar) findViewById(R.id.my_toolbar);
    }

    private void initialiseUI() {

        if (isConnectedToInternet()) {
            final String query = "?q=subject:fiction&langRestrict=en";
            new DownloadHorizontalView(findViewById(android.R.id.content), getApplicationContext()).execute(query);

            final TextView category = findViewById(R.id.categoryTV);
            category.setText("Fiction");

            TextView seeAll = findViewById(R.id.see_allTV);
            seeAll.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    // create intent and attach clicked food item object to it
                    Intent intent = new Intent(getApplicationContext(), BooksListActivity.class);

                    intent.putExtra("Query", query);
                    intent.putExtra("Category", category.getText());
                    startActivity(intent);
                }
            });

        } else {

            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

            alert.setTitle("Problem!")
                    .setMessage("No internet connection")
                    .setCancelable(false)
                    .setNeutralButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                        }
                    });
            alert.create();
            alert.show();

            System.out.println("No internet connection");
        }

    }

    public boolean isConnectedToInternet(){
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        System.out.println("Conn: " + isConnected);
        return isConnected;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {

            // create intent and attach clicked food item object to it
            Intent intent = new Intent(getApplicationContext(), ToReadActivity.class);

            startActivity(intent);
//            Toast.makeText(MainActivity.this, "Action clicked", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}