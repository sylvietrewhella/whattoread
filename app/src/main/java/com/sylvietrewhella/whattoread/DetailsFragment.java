package com.sylvietrewhella.whattoread;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.IndustryIdentifier;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {

    private GoogleBook book;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_details, container, false);

        BookDetail detailsActivity = (BookDetail) getActivity();
        this.book = detailsActivity.getBook();

        initialiseUI(view);

        return view;
    }

    public void initialiseUI(View v) {
        TextView title = v.findViewById(R.id.titleTV);
        title.setText(book.getVolInfo().getTitle());

        TextView publisher = v.findViewById(R.id.publisherTV);
        String pub = publisher.getText().toString();
        publisher.setText(pub + book.getVolInfo().getPublisher());

        TextView date = v.findViewById(R.id.dateTV);
        String sDate = date.getText().toString();

        if (book.getVolInfo().getPublishedDate() != null) {
            date.setText(sDate + book.getVolInfo().getPublishedDate());
        } else {
            date.setText(sDate + " Unknown");
        }


        TextView language = v.findViewById(R.id.languageTV);
        String sLang = language.getText().toString();
        language.setText(sLang + book.getVolInfo().getLanguage());



        TextView isbn10 = v.findViewById(R.id.isbn10);
        TextView isbn13 = v.findViewById(R.id.isbn13);

        if (book.getVolInfo().getIndustryIdentifiers() != null) {
            IndustryIdentifier isbn10value = book.getVolInfo().getIndustryIdentifiers().get(1);
            IndustryIdentifier isbn13value = book.getVolInfo().getIndustryIdentifiers().get(0);

            if (isbn10value != null) {
                isbn10.setText(isbn10value.toString());
            }
            if (isbn13value != null) {
                isbn13.setText(isbn13value.toString());
            }

        } else {
            isbn10.setText(sDate + " Unknown");
            isbn13.setText(sDate + " Unknown");
        }

        TextView details = v.findViewById(R.id.detailsTV);
        details.setText(book.getVolInfo().getDescription());
    }


}
