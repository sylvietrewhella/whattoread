package com.sylvietrewhella.whattoread;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * This class is a utility class which defines methods to manupilate
 * and interact with user inputs
 *
 * @author (Sylvie Trewhella)
 * @version (24.10.2018)
 */
public class IOUtility {

    private static Scanner in = new Scanner(System.in);

    /**
     * getString(String prompt) prompts the user for an input,
     * reads it as a String object and returns it.
     *
     * @param   prompt  A String which prompts for user input
     * @return  String  An object which holds the user input
     */
    public static String getString(String prompt)
    {
        System.out.print(prompt + " ");
        return in.nextLine();
    }

    /**
     * getInteger(String prompt) prompts the user for an input
     * and parses the input to an Integer object.
     *
     * @param   prompt  A String which prompts for user input
     * @return  Integer An object which holds the user input
     */
    public static Integer getInteger(String prompt)
    {
        Integer i = 0;
        while(true)
        {
            try
            {
                System.out.print(prompt + " ");
                i = Integer.parseInt(in.nextLine());
                break;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid Integer");
            }
        }
        return i;
    }

    /**
     * getInteger() prompts the user for an integer input and
     * parses the input to an Integer object.
     *
     * @return Integer  An object which holds the user input
     */
    public static Integer getInteger()
    {
        Integer i = 0;
        while(true)
        {
            try
            {
                System.out.print("Please enter an integer");
                i = Integer.parseInt(in.nextLine());
                break;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid Integer");
            }
        }
        return i;
    }


    /**
     * getDouble(String prompt) prompts the user for an input
     * and parses the input to Double object.
     *
     * @param   prompt  A String which prompts for user input
     * @return  Double  An object which holds the user input
     */
    public static Double getDouble(String prompt)
    {
        Double d = 0.00;
        while(true)
        {
            try
            {
                System.out.print(prompt + " ");
                d = Double.parseDouble(in.nextLine());
                break;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid Double");
            }
        }
        return d;
    }


    /**
     * stringToDate(String date) converts a String into
     * a date object in (yyyy-MM-dd) format
     *
     * @param   date  A String od date
     * @return  Date  A Date object in yyyy-MM-dd format
     */
    public static Date stringToDate(String date) {
        try {
            return new SimpleDateFormat("yyyy").parse(date);
        } catch (java.text.ParseException e) {
            return null;
        }

    }


    /**
     * dateToString(Date date) converts a date object into
     * a string
     *
     * @param   date  A date object
     * @return  String date as a string
     */
    public static String dateToString(Date date) {

        DateFormat df = new SimpleDateFormat("yyyy");
        String stringDate = df.format(date);

        return stringDate;

    }

    /**
     * getCurrentDate() gets a current date as a Date object
     */
    public static Date getCurrentDate() {

        return Calendar.getInstance().getTime();
    }


    public static Bitmap getBitmapFromURL(String url) {
        URL ulrn = null;
        InputStream is = null;
        try {
            ulrn = new URL(url);
            HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
            is = con.getInputStream();

        }
        catch (MalformedURLException e) {
            e.printStackTrace();

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return BitmapFactory.decodeStream(is);
    }

    /**
     * Append input (string) to a file
     * @param context
     * @param data
     * @param fileName
     */
    static void appendInput(Context context, String data, String fileName) {
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(fileName, Context.MODE_APPEND);
            outputStream.write(data.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get contents of a file
     * @param fis InputStream object file
     * @return Array list of locations from a file
     */
    public static ArrayList<String> getFileContents(InputStream fis) {
        ArrayList<String> booksFromFile = new ArrayList();
        if(fis != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                String line = br.readLine();
                while (line != null) {
                    booksFromFile.add(line);
                    line = br.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("fis null in IO");

        }
        return booksFromFile;
    }

    /**
     * Get file from raw resources as InputStream object
     * @param context
     * @return InputStream object
     */
    public static InputStream getFile(Context context, String fileName) {
        InputStream fileInput = null;
        try {
            File storageFile = new File(context.getFilesDir(), fileName);
            storageFile.createNewFile();
            fileInput = context.openFileInput(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //fileInput = context.getResources().openRawResource(R.raw.au_locations);
        return fileInput;
    }

    /**
     * Get contents of a file
     * @param fis InputStream object file
     * @return Array list of locations from a file
     */
    public static Boolean removeLineFromFile(InputStream fis, String bookStr, File originalFile, Context context) {

        boolean successful = false;

        if(fis != null) {

            try {

                File tempFile = new File(context.getFilesDir(),"temp_file");

                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                String line;
                while ((line = br.readLine())!= null) {
                    String trimmedLine = line.trim();

                    if (trimmedLine.equals(bookStr)) {
                        continue;
                    } else {
                        appendInput(context, line,"temp_file");
                    }

                }
                br.close();

                boolean deleted = context.deleteFile("to_read_books");

                System.out.println("Deleted: " + deleted);


                File original = new File(context.getFilesDir(),"to_read_books");

                successful = tempFile.renameTo(original);

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else {
            System.out.println("fis null in IO");
        }

        System.out.println(successful);
        return successful;
    }


}