package com.sylvietrewhella.whattoread;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;


/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends Fragment {

    private GoogleBook book;


    public OverviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_overview, container, false);

        BookDetail detailsActivity = (BookDetail) getActivity();
        this.book = detailsActivity.getBook();

        initialiseUI(view);

        return view;

    }

    private void initialiseUI(View v) {

        TextView title = (TextView) v.findViewById(R.id.titleTV);
        title.setText(book.getVolInfo().getTitle());

        TextView author = (TextView) v.findViewById(R.id.authorTVdetail);
        author.setText(book.getVolInfo().getAuthors().get(0));

        TextView rating = (TextView) v.findViewById(R.id.ratingTVdetail);

        Double ratingValue = book.getVolInfo().getAverageRating();

        if (ratingValue != null) {
            rating.setText(ratingValue.toString());
        }
        else {
            rating.setText("No Ratings yet");
        }

        TextView details = (TextView) v.findViewById(R.id.plotTVdetail);
        details.setText(book.getVolInfo().getDescription());

        String imageUrl = book.getVolInfo().getImageLinks().getSmallThumbnail();
        ImageView thumbnail = (ImageView) v.findViewById(R.id.imageVdetail);

        Picasso.get().load(imageUrl).into(thumbnail);
    }

}
