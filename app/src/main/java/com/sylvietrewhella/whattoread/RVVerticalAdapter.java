package com.sylvietrewhella.whattoread;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;
import com.sylvietrewhella.whattoread.GoogleBooksService.BooksManager;


import java.util.List;


/**
 * This is an adapter for Books. RecyclerViews have different methods, but essentially cover
 * all that we did for the ViewHolder (with stashing and retrieving views, and a separate
 * method for binding).
 */
public class RVVerticalAdapter extends RecyclerView.Adapter<RVVerticalAdapter.ViewHolder> {

    private List<GoogleBook> booksList;
    private Context context;
    private List<GoogleBook> readList;
    private View view;
    private RecyclerView rvBooks;


    public RVVerticalAdapter(List<GoogleBook> booksList, Context context, RecyclerView rv){
        this.booksList= booksList;
        this.context = context;
        this.readList = BooksManager.getBooksToRead(context);
        if (this.booksList != null) {
            System.out.println("books size: " + booksList.size());
        } else {
            System.out.println("books list is null");
        }
        this.rvBooks = rv;

    }

    /**
     * This method creates a ViewHolder if needed.
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RVVerticalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        this.view = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row, parent, false); //adds custom row xml layout
        ViewHolder vh = new ViewHolder(this.view);
        return vh;
    }


    /**
     * This is the binding code.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final RVVerticalAdapter.ViewHolder holder, final int position) {

        final int i = position;


        String bookTitle = booksList.get(i).getVolInfo().getTitle();
        Double bookRating = booksList.get(i).getVolInfo().getAverageRating();

        holder.mTextView.setText(bookTitle);

        if (bookRating != null) {
            holder.mRatingView.setText(bookRating.toString());
        } else {
            holder.mRatingView.setText(" No ratings yet.");
        }

//        System.out.println("Before checking list");


        GoogleBook book = booksList.get(i);

        if (BooksManager.isOnToReadList(book, readList)) {
            System.out.println(bookTitle + " is on list");
            holder.mToggle.setChecked(true);
        }





        String imageUrl = booksList.get(position).getVolInfo().getImageLinks().getSmallThumbnail();
        // set image using Picasso - from thumbnail url in the book object
        Picasso.get().load(imageUrl).into(holder.mImage);

        holder.mImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String bookTitle = booksList.get(i).getVolInfo().getTitle();

                // create intent and attach clicked food item object to it
                Intent intent = new Intent(context.getApplicationContext(), BookDetail.class);

                Gson gson = new Gson();
                String bookString = gson.toJson(booksList.get(i));
                intent.putExtra("Book", bookString);

                // start Book Detail activity and expect result
                context.startActivity(intent);
            }
        });


        holder.mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {


                Gson gson = new Gson();
                String bookString = gson.toJson(booksList.get(i));
                String name = booksList.get(i).getVolInfo().getTitle();


////




                if (!BooksManager.isOnToReadList(booksList.get(position), readList)) {

                    IOUtility.appendInput(context.getApplicationContext(), bookString, "to_read_books");


                    Snackbar.make(rvBooks, "Book " + name + " added.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else {


                    BooksManager.removeFromList(bookString, context.getApplicationContext());
                    holder.mToggle.setChecked(false);
                    Snackbar.make(rvBooks, "Book " + name + " removed.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                }

            }
        });
    }




    @Override
    public int getItemCount() {

        return booksList.size();
    }


    /**
     * View holder class
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public TextView mRatingView;
        public ImageView mImage;
        public ToggleButton mToggle;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.item_title);
            mRatingView = v.findViewById(R.id.item_rating);
            mImage = v.findViewById(R.id.item_image);
            mToggle = v.findViewById(R.id.toggleButton);
        }
    }
}
