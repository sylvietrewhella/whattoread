package com.sylvietrewhella.whattoread.GoogleBooksService.Books;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Sale info of a google book.
 * @author Sylvie Trewhella
 * @version (24.10.2018)
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaleInfo {


    private String country;
    private String saleability;

    /**
     * Get info of book sales
     * @return string of book sales information
     */
    public String toString() {
        return this.country + ", " +  this.saleability;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSaleability() {
        return saleability;
    }

    public void setSaleability(String saleability) {
        this.saleability = saleability;
    }

}