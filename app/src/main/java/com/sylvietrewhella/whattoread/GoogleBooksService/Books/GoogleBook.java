package com.sylvietrewhella.whattoread.GoogleBooksService.Books;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Google book object
 * @author Sylvie Trewhella
 * @version 18.4.2018
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleBook {

    @JsonProperty("volumeInfo")
    private VolumeInfo volInfo;
    //public SaleInfo saleInfo;



    /**
     * GoogleBook info
     * @return sting of book info
     */
    public String toString() {
        return this.volInfo.toString(); // + ", " + this.saleInfo;
    }


    public VolumeInfo getVolInfo() {
        return volInfo;
    }

    public void setVolInfo(VolumeInfo volInfo) {
        this.volInfo = volInfo;
    }
}