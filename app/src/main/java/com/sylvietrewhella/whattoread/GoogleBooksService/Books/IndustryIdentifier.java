package com.sylvietrewhella.whattoread.GoogleBooksService.Books;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Industry identifiers of a google book (ISBNs)
 * @author Sylvie Trewhella
 * @version (24.10.2018)
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndustryIdentifier {



    private String type;
    private String identifier;

    /**
     * Get industry identifiers
     * @return string of industry identifiers
     */
    public String toString() {
        return this.type + ", " +  this.identifier;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
