package com.sylvietrewhella.whattoread.GoogleBooksService;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;


/**
 * Google book response class for deserilising json response
 * @author Sylvie Trewhella
 * @version (24.10.2018)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleBookResponse {

    public int totalItems;
    public List<GoogleBook> items;

}
