package com.sylvietrewhella.whattoread.GoogleBooksService.Books;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Google books volume info
 * @author Sylvie Trewhella
 * @version 18.4.2018
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeInfo {

    private String title;
    private List<String> authors;
    private String publisher;
    private String publishedDate;
    private String description;
    private List<IndustryIdentifier> industryIdentifiers;
    private Double averageRating;
    private String language;
    private ImageLinks imageLinks;


    /**
     * Get volume info information
     * @return string of volume information
     */
    public String toString() {
//        if (title != null &&  authors.get(0) != null && this.publisher != null && this.publishedDate != null && this.industryIdentifiers.get(0) != null && this.industryIdentifiers.get(1) != null && this.averageRating != null && this.language != null && this.imageLinks != null) {
//            return this.title + ", " +  this.authors.get(0) + ", " + this.publisher + ", " + this.publishedDate + ", " + this.industryIdentifiers.get(0) + ", " + this.industryIdentifiers.get(1) + ", " + this.averageRating + ", " + this.language + ", " + this.imageLinks;
//        }
//        else {
            return authors.get(0) + ": " + title;
        //}
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<IndustryIdentifier> getIndustryIdentifiers() {
        return industryIdentifiers;
    }

    public void setIndustryIdentifiers(List<IndustryIdentifier> industryIdentifiers) {
        this.industryIdentifiers = industryIdentifiers;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ImageLinks getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(ImageLinks imageLinks) {
        this.imageLinks = imageLinks;
    }


}

