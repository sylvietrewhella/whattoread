package com.sylvietrewhella.whattoread.GoogleBooksService;

import android.content.Context;

import com.google.gson.Gson;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.IndustryIdentifier;
import com.sylvietrewhella.whattoread.IOUtility;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class BooksManager {

    /**
     * Gets books from a txt file
     */
    public static ArrayList<GoogleBook> getBooksToRead(Context context)
    {
        System.out.println("Reading from file");

        ArrayList<GoogleBook> booksFromFile = new ArrayList<GoogleBook>();
        String fileName = "to_read_books";
        InputStream fis = IOUtility.getFile(context, fileName);
        ArrayList<String> data = IOUtility.getFileContents(fis);

        for(String str : data)
        {
            Gson gson = new Gson();
            GoogleBook newBook = gson.fromJson(str, GoogleBook.class);
            booksFromFile.add(newBook);
            System.out.println("Book Reading: " + newBook);

        }

        return booksFromFile;
    }

    public static Boolean isOnToReadList(GoogleBook book, List<GoogleBook> books) {

        Boolean isOnList = false;

        List identifier = book.getVolInfo().getIndustryIdentifiers();

        for (GoogleBook b : books) {

            List currentIdentifier = b.getVolInfo().getIndustryIdentifiers();

            if (identifier != null && currentIdentifier != null) {


                String isbn = currentIdentifier.get(0).toString();
                String isbnBook = identifier.get(0).toString();

                System.out.println("ISBNs" + isbn + " " + isbnBook);


                if (isbn.equals(isbnBook)) {
                    isOnList = true;
                }

            } else {

                System.out.println("ISBNs are empty");

                String title = book.getVolInfo().getTitle();
                String bookTitle = b.getVolInfo().getTitle();

                if (title.equals(bookTitle)) {
                    isOnList = true;
                }

            }
        }

        System.out.println(book + " " + isOnList.toString());

        return isOnList;
    }

    public static void removeFromList(String toRemoveLine, Context context) {

        String fileName = "to_read_books";
        InputStream fis = IOUtility.getFile(context, fileName);
        File originalFile = new File(fileName);

        IOUtility.removeLineFromFile(fis, toRemoveLine, originalFile, context);
    }
}
