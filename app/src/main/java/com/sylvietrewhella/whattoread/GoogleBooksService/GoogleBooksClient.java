package com.sylvietrewhella.whattoread.GoogleBooksService;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GoogleBooksClient {


    private final String SEARCH_URL = "https://www.googleapis.com/books/v1/volumes";

    private String jsonResponse;
    private GoogleBookResponse bookResponse;


    /**
     * Send get request t
     * @param requestUrl
     * @return
     */
    public String sendGetRequest(String requestUrl) {

        StringBuffer response = new StringBuffer();

        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            connection.connect();

            InputStream stream = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader in = new BufferedReader(reader);

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
            connection.disconnect();

            jsonResponse = response.toString();

            if (response.toString().endsWith(": 0}")) {
                System.out.println("This search has no matches.");
                return null;
            }

            ObjectMapper mapper = new ObjectMapper();
            bookResponse = mapper.readValue(this.jsonResponse, GoogleBookResponse.class);

            if (bookResponse.items != null) {
                System.out.println("Book Response: " + bookResponse.items.size());

                for (GoogleBook book : bookResponse.items ) {
                    System.out.println(book.toString());
                }


            }
            else {
                System.out.println("Book Response items is null: ");
            }


        } catch (JsonParseException e) {
            System.out.println("\nError: JsonParseException");
            e.printStackTrace();
        } catch (JsonMappingException e) {
            System.out.println("\nError: JsonMappingException");
            e.printStackTrace();
        } catch (MalformedURLException e) {
            System.out.println("\nURL EXCEPTION in creating URL object");
            e.printStackTrace();

        } catch (IOException e) {
            System.out.println("\nIO EXCEPTION in url open connection");
            e.printStackTrace();
        }

        return response.toString();
    }

    public String searchBooks(String query) {

        String requestURL = SEARCH_URL + query;
        System.out.println("URL: " + requestURL);

        return sendGetRequest(requestURL);
    }


    /**
     * GETTERS
     */

    /**
     * gets json response
     * @return json string (book)
     */
    public String getJsonResponse() {
        return jsonResponse;
    }

    /**
     * gets googlebookresponse object
     * @return book
     */
    public GoogleBookResponse getBookResponse() {
        return bookResponse;
    }
}
