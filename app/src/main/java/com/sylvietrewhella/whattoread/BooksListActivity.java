package com.sylvietrewhella.whattoread;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.sylvietrewhella.whattoread.AsyncTaskProcess.DownloadVerticalView;

public class BooksListActivity extends AppCompatActivity {


    private Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_list);
        initialiseUI();
    }

    private void initialiseUI() {
        //books = Books.get(getApplicationContext());
        this.data = getIntent().getExtras();


        TextView heading = findViewById(R.id.headingTVList);
        heading.setText(data.getString("Category"));

        String query = data.getString("Query") + "&maxResults=40";
        new DownloadVerticalView(findViewById(android.R.id.content), getApplicationContext()).execute(query);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {

            // create intent and attach clicked food item object to it
            Intent intent = new Intent(getApplicationContext(), ToReadActivity.class);

            startActivity(intent);
//            Toast.makeText(MainActivity.this, "Action clicked", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
