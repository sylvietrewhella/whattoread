package com.sylvietrewhella.whattoread;



import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;
import java.util.List;


public class RVHorizontalAdapter extends RecyclerView.Adapter<RVHorizontalAdapter.BooksViewHolder>{
    private List<GoogleBook> horizontalBooksList;
    Context context;


    public RVHorizontalAdapter(List<GoogleBook> horizontalBooksList, Context context){
        this.horizontalBooksList= horizontalBooksList;
        this.context = context;
    }

    @Override
    public BooksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View booksView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_item, parent, false);
        BooksViewHolder booksVH = new BooksViewHolder(booksView);
        return booksVH;
    }

    @Override
    public void onBindViewHolder(BooksViewHolder holder, final int position) {

        String imageUrl = horizontalBooksList.get(position).getVolInfo().getImageLinks().getSmallThumbnail();
        // set image using Picasso - from thumbnail url in the book object
        Picasso.get().load(imageUrl).into(holder.imageView);
        holder.author.setText(horizontalBooksList.get(position).getVolInfo().getAuthors().get(0));
        holder.title.setText(horizontalBooksList.get(position).getVolInfo().getTitle());

        holder.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String bookTitle = horizontalBooksList.get(position).getVolInfo().getTitle();
               // GoogleBook book = ;

                // create intent and attach clicked food item object to it
                Intent intent = new Intent(context.getApplicationContext(), BookDetail.class);

                Gson gson = new Gson();
                String bookString = gson.toJson(horizontalBooksList.get(position));
                System.out.println("JSON FROM BOOK: " + bookString);
                System.out.println("JSON PRINTED");

                //String jsonInString = "{'name' : 'mkyong'}";


                intent.putExtra("Book", bookString);

                // start FormActivity and expect result
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalBooksList.size();
    }

    public class BooksViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title;
        TextView author;

        public BooksViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.bookImage);
            title = view.findViewById(R.id.bookTitle);
            author = view.findViewById(R.id.bookAuthor);
        }
    }
}

