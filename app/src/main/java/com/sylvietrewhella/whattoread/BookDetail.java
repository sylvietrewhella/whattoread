package com.sylvietrewhella.whattoread;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;

public class BookDetail extends AppCompatActivity {

    private Bundle data;
    private GoogleBook book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getIntentData();

        // start with overview fragment
        loadFragment(new OverviewFragment());
    }

    /**
     * Handling clicks on the bottom navigation menu. For each item, a fragment is created and
     * put into the frame for the fragment.
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_details:
                    fragment = new DetailsFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_overview:
                    fragment = new OverviewFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getIntentData() {

        this.data = getIntent().getExtras();

        Gson gson = new Gson();
        GoogleBook newBook = gson.fromJson(data.getString("Book"), GoogleBook.class);
        System.out.println("\nNEW BOOK IN DETAIL: " + newBook.toString());

        this.book = newBook;
    }


    public GoogleBook getBook() {
        return book;
    }
}
