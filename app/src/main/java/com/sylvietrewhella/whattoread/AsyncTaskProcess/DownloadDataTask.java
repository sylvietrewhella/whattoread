package com.sylvietrewhella.whattoread.AsyncTaskProcess;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sylvietrewhella.whattoread.GoogleBooksService.Books.GoogleBook;
import com.sylvietrewhella.whattoread.GoogleBooksService.GoogleBooksClient;
import com.sylvietrewhella.whattoread.R;

import java.util.List;


/** Background thread to process loading and not blocking the UI */
public class DownloadDataTask extends AsyncTask<String, String, String> {


    private GoogleBooksClient client;
    private View v;
    private List<GoogleBook> books;
    private Context context;

    public DownloadDataTask(View v, Context context) {
        this.v = v;
        this.books = null;
        this.client = new GoogleBooksClient();
        this.context = context;
    }


    protected String doInBackground(String... urls) {

        String jsonResponse = client.searchBooks(urls[0]);
//            System.out.println(this.jsonResponse);
        books = client.getBookResponse().items;
        publishProgress();
        return jsonResponse;
    }

    protected void onProgressUpdate() {

    }


    public GoogleBooksClient getClient() {
        return client;
    }

    public void setClient(GoogleBooksClient client) {
        this.client = client;
    }

    public View getV() {
        return v;
    }

    public void setV(View v) {
        this.v = v;
    }

    public List<GoogleBook> getBooks() {
        return books;
    }

    public void setBooks(List<GoogleBook> books) {
        this.books = books;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}

