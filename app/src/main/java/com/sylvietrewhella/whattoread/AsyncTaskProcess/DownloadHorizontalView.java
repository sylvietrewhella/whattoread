package com.sylvietrewhella.whattoread.AsyncTaskProcess;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sylvietrewhella.whattoread.R;
import com.sylvietrewhella.whattoread.RVHorizontalAdapter;

public class DownloadHorizontalView extends DownloadDataTask {

    ProgressBar progress;

    public DownloadHorizontalView(View v, Context context) {
        super(v, context);
        this.progress = v.findViewById(R.id.progressBar);
    }

    @Override
    protected void onProgressUpdate() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.setVisibility(View.GONE);
        createHorizontalRecycleView();
    }

    private void createHorizontalRecycleView() {
        RecyclerView horizontalView = super.getV().findViewById(R.id.recyclerViewHorizontal);
        // add a divider after each item for more clarity
//        horizontalView.addItemDecoration(new DividerItemDecoration(super.getContext(), LinearLayoutManager.HORIZONTAL));
        RVHorizontalAdapter booksAdapter = new RVHorizontalAdapter(super.getBooks(), super.getV().getContext());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(super.getContext(), LinearLayoutManager.HORIZONTAL, false);
        horizontalView.setLayoutManager(horizontalLayoutManager);
        horizontalView.setAdapter(booksAdapter);
    }


}
