package com.sylvietrewhella.whattoread.AsyncTaskProcess;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sylvietrewhella.whattoread.R;
import com.sylvietrewhella.whattoread.RVVerticalAdapter;

public class DownloadVerticalView extends DownloadDataTask {

    ProgressBar progress;

    public DownloadVerticalView(View v, Context context) {
        super(v, context);
        this.progress = v.findViewById(R.id.progressBar2);
    }

    @Override
    protected void onProgressUpdate() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(String result) {
        progress.setVisibility(View.GONE);
        createVerticalRecycleView();
    }

    private void createVerticalRecycleView() {
        RecyclerView verticalView = super.getV().findViewById(R.id.booksView);
        // add a divider after each item for more clarity
        verticalView.addItemDecoration(new DividerItemDecoration(super.getContext(), LinearLayoutManager.VERTICAL));
        RVVerticalAdapter booksAdapter = new RVVerticalAdapter(super.getBooks(), super.getV().getContext(), verticalView);
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(super.getContext(), LinearLayoutManager.VERTICAL, false);
        verticalView.setLayoutManager(verticalLayoutManager);
        verticalView.setAdapter(booksAdapter);
    }
}
