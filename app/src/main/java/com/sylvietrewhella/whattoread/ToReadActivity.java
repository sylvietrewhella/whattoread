package com.sylvietrewhella.whattoread;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.sylvietrewhella.whattoread.GoogleBooksService.BooksManager;

public class ToReadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_read);
        initialiseUI();
    }

    private void initialiseUI() {

        TextView heading = findViewById(R.id.heading);
        heading.setText("My List");
        RecyclerView verticalView = findViewById(R.id.booksToReadView);
        // add a divider after each item for more clarity
        verticalView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));

        RVVerticalAdapter booksAdapter = new RVVerticalAdapter(BooksManager.getBooksToRead(getApplicationContext()), getApplicationContext(), verticalView);
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        verticalView.setLayoutManager(verticalLayoutManager);
        verticalView.setAdapter(booksAdapter);
    }






}
